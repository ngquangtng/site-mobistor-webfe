'use strict';

var QLSVServices = angular.module('QLSVServices', ['ngCookies']);

QLSVServices.factory('APIServer', function() {
    return {
        host: 'http://localhost:47703'
    };
})

.factory('AuthenticationService',
    ['$http', '$cookies', '$rootScope', '$timeout', 'APIServer', '$log', '$httpParamSerializerJQLike',
    function ($http, $cookies, $rootScope, $timeout, APIServer, $log, $httpParamSerializerJQLike) {
        var service = {};

        service.Login = function (username, password, successCallback, errorCallback) {
            var data = {username : username, password : password, grant_type : 'password'};

            $http({
                method: 'POST',
                url: APIServer.host + '/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                data: $httpParamSerializerJQLike(data),
            }).then(successCallback, errorCallback);

        };

        service.SetCredentials = function (username, accessToken) {

            $rootScope.globals = {
                currentUser: {
                    username: username,
                    accessToken: accessToken
                }
            };

            $http.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
            $cookies.putObject('globals', $rootScope.globals);
        };

        service.ClearCredentials = function () {
            $rootScope.globals = {};
            $cookies.remove('globals');
            $log.log($http.defaults.headers.common);
            delete $http.defaults.headers.common.Authorization;
        };

        return service;
}]);