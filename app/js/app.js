'use strict';

/* App Module */

var QLSVApp = angular.module('QLSVApp', [
  'ngRoute',
  'ngCookies',
  'QLSVControllers',
]);

QLSVApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/dangnhap', {
        templateUrl: 'partials/dangnhap.html',
        controller: 'DangNhapCtrl'
      }).
      when('/sinhvien', {
        templateUrl: 'partials/danhsach-sinhvien.html',
        controller: 'DanhSachSinhVienCtrl'
      }).
      when('/sinhvien/them',{
        templateUrl: 'partials/them-sinhvien.html',
        controller: 'ThemSinhVienCtrl'
      }).
      when('/sinhvien/:mssv', {
        templateUrl: 'partials/chitiet-sinhvien.html',
        controller: 'ChiTietSinhVienCtrl'
      }).
      when('/sinhvien/capnhat/:mssv',{
        templateUrl: 'partials/capnhat-sinhvien.html',
        controller: 'CapNhatSinhVienCtrl'
      }).
      otherwise({
        redirectTo: '/dangnhap'
      });
  }])
.run(['$rootScope', '$location', '$cookies', '$http', '$log', 'AuthenticationService',
  function ($rootScope, $location, $cookies, $http, $log, AuthenticationService) {
      $rootScope.logout = function() {
        AuthenticationService.ClearCredentials();
      };

      // keep user logged in after page refresh
      $rootScope.globals = $cookies.getObject('globals') || {};
      if ($rootScope.globals.currentUser) {
          $http.defaults.headers.common.Authorization = 'Bearer ' + $rootScope.globals.currentUser.accessToken;
      }



      $rootScope.$on('$locationChangeStart', function (event, next, current) {
          // redirect to login page if not logged in
          if ($location.path() !== '/dangnhap' && !$rootScope.globals.currentUser) {
              $location.path('/dangnhap');
          }
      });
  }]);