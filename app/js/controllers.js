'use strict';

/* Controllers */

var QLSVControllers = angular.module('QLSVControllers', ['QLSVServices']);

QLSVControllers.controller('DangNhapCtrl', ['$scope', '$rootScope', '$location', 'AuthenticationService', '$log',
	function ($scope, $rootScope, $location, AuthenticationService, $log) {
		AuthenticationService.ClearCredentials();
		$scope.submit = function() {
			$scope.disable = true;
			AuthenticationService.Login($scope.username, $scope.password,
				function(response) {
                    AuthenticationService.SetCredentials($scope.username, response.data.access_token);
                    $location.path('/sinhvien');
                    $scope.disable = false;
            	},
            	function(response) {
                    $scope.error = 'Tên đăng nhập hoặc mật khẩu không đúng';
                    $scope.disable = false;
            	}
            );
		};
}]);


QLSVControllers.controller('DanhSachSinhVienCtrl', ['$scope', '$http', 'APIServer', '$log',
	function ($scope, $http, APIServer, $log) {

		$http.get(APIServer.host + '/api/sinhvien/tatca'
		).success(function (data){
			$scope.data = data;
		});

		$scope.xoaSinhVien = function(mssv) {
			$http.delete(APIServer.host + '/api/sinhvien/' + mssv)
			.then(function (response){
				alert('Xóa thành công');
			},
			function (response) {
				if(response.status == 400) {
					alert('Mã số sinh viên này không tồn tại');
				} else {
					alert('Đã có lỗi xảy ra');
				}
			});
		};

}]);

QLSVControllers.controller('ChiTietSinhVienCtrl', ['$scope', '$http','$routeParams', 'APIServer',
	function ($scope, $http, $routeParams, APIServer) {
		var mssv = $routeParams.mssv;
		$http.get(APIServer.host + '/api/sinhvien/' + mssv)
			.success(function (data){
				$scope.data = data;
			});
	}
]);

QLSVControllers.controller('ThemSinhVienCtrl', ['$scope', '$http', 'APIServer',
	function ($scope, $http, APIServer) {
		$scope.submit = function(sinhVien) {
			var sv = sinhVien;
			$http.post(APIServer.host + '/api/sinhvien/', sv)
				.then(function (response){
					$scope.data = response.data;
					alert('Thêm thành công');
				},
				function (response) {
					if(response.status == 409) {
						alert('Mã số sinh viên này đã tồn tại');
					} else if (response.status == 401) {
						alert('Bạn phải đăng nhập bằng tài khoản admin');
					}
					else
					{
						alert('Đã có lỗi xảy ra');
					}
				});
		};
	}
]);
QLSVControllers.controller('CapNhatSinhVienCtrl', ['$scope', '$http', '$routeParams', 'APIServer',
	function ($scope, $http, $routeParams, APIServer) {
		var mssv = $routeParams.mssv;
		$http.get(APIServer.host + '/api/sinhvien/' + mssv)
			.success(function (data){
				$scope.sinhVien = data;
			});

		$scope.submit = function(sinhVien) {
			var sv = sinhVien;
			$http.put(APIServer.host + '/api/sinhvien/'+ sv.MaSV, sv)
				.then(function (response){
					$scope.data = response.data;
					alert('Cập nhật thành công');
				},
				function (response) {
					if (response.status == 401) {
						alert('Bạn phải đăng nhập bằng tài khoản admin');
					} else {
						alert('Đã có lỗi xảy ra');
					}
				});
		};
	}
]);
